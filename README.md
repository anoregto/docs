API da Central de Informações dos Cartórios
================================================================

Esse repositório foicriado para descrever a API de comunicação com a Central de Informações dos Cartórios.

As diferentes versões da documentação serão *TAGs* desse repositório, assim possibilitando que sejam vistas com as respectivas datas de publicação.

A area de *Issues* desse repositório é destinada a esclarecimentos e discussões de problemas relacionados a API, bem como para reportar problemas.

O Arquivo API.md contém as especificações da API de acordo com o formato exposto em [REST API Documentation Best Practices](https://bocoup.com/weblog/documenting-your-api).
Segundo esse documento cada End-Point deve ser descrito contendo:

# Preparar a documentação

A documentação deve estar disponível no arquivo `API.md` na raiz do repositórios, para organizar os documentos eles estão separados e ordenados na pasta `docs/`, para concatena-los basta usar o comando padrão do grunt.

	npm install # caso ainda não tenha sido instalado
	grunt

# Informações por End-Point

## Titulo

O nome da chamada da API.

Exemplo: Encontrar Todos os Usuário

## URL: o caminho para chamar a url.

`/users` or `/users/:id` or `/users?id=:id`

## Método

O tipo da requisição.

    GET | POST | DELETE | PUT

## Parametros de URL

Os parametros informados por url, caso existam.

*Required* 

    id=[integer] 

example: id=12 

*Optional*

    photo_id=[alphanumeric] 

example: photo_id=2345kj3 

## Parametros: os parametros informados no corpo da requisição, caso existam. Se possível com exemplos de requisições.

*Exemplo*

	{
	  u : {
	    email : [string],
	    nome : [string],
	    tipo_senha : [alphanumeric]
	    senha : [alphanumeric],
	    confirmacao_senha : [alphanumeric]
	  }
	}

*Exemplo*

	{
	  u : {
	    email : "john@smith.com",
	    nome : "John",
	    tipo_senha : "apassw0rd"
	    senha : "anewpassw0rd",
	    confirmacao_senha : "anewpassw0rd"
	  }
	}

## Resposta de Sucesso

Qual o código de sucesso e quais os dados retornados.

Exemplo:

*Código:* 200 
*Conteúdo:* `{ id : 12 }`

* Resposta de Erro: Qual o código de sucesso e quais os dados retornados. Lembrando que poderão haver vários erros.

Exemplo:

*Código:* 401 UNAUTHORIZED 
*Conteúdo:* { error : "Log in" }

*OU*

*Código:* 422 Unprocessable Entry 
*Conteúdo:* { error : "Email invalid" }

## Exemplo de Chamada

Um exemplo simples de como acionar o end-point. Será utilizado o padrão angular $http para os exemplos.


    $http.post(API + '/gise/selo', {
    	// dados aqui
    });


## Observações

Eventuais considerações e observações a respeito do end-point.

