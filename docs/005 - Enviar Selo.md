# Enviar Selo

### Url

`/selo/enviar`

### Método

`POST`

### Parametros

* token - O token de acesso do cartório;
* tipo - O tipo do ato praticado de acordo com tipos do GISE;
* selo - O código completo do selo separado por hífen;
* atos - Uma lista dos atos praticados naquele selo, contendo:
  * codigo - O código do ato segundo código do GISE;
  * dados - Os dados do ato de acordo com o serviço Listar Propriedades do Ato.

    {
        "token": "abcdefghijklmnopqrstuvwxyz",
        "tipo": "AUTENTICACAO",
        "selo": "222222AAA000001-AAA",
        "atos": [{
            "codigo": 1004,
            "dados": [{
                "campo": "tipo_ato_cep",
                "valor": "2"
            },{
                "campo": "diferido",
                "valor": "false"
            },{
                "campo": "gratuito",
                "valor": "false"
            },{
                "campo": "livro",
                "valor": "2"
            }, {
                "campo": "folha",
                "valor": "65"
            }, {
                "campo": "observacao",
                "valor": "preenchi porque está obrigatório"
            }, {
                "campo": "valor_issqn",
                "valor": ""
            }, {
                "campo": "data_protocolo",
                "valor": "2017-02-06"
            }, {
                "campo": "data_ato",
                "valor": "2017-02-06"
            }, {
                "campo": "quantidade_pessoas",
                "valor": "1"
            }, {
                "campo": "detalhe",
                "valor": [{
                  "dados":[
                    {
                        "campo": "cpf",
                        "valor": "51715386388"
                    }, {
                        "campo": "qualidade",
                        "valor": "2"
                    }, {
                        "campo": "nome",
                        "valor": "Fulano da Silva"
                    }, {
                        "campo": "tipo_documento_1",
                        "valor": "CNPJ"
                    }, {
                        "campo": "numero_documento_1",
                        "valor": "854695"
                    }, {
                        "campo": "rg",
                        "valor": "563254"
                    }, {
                        "campo": "rg_orgao_emissor",
                        "valor": "SSP-TO"
                    }]
                }]
            }]
        }]
    }

Note que existem valores complexos como envolvidos, que é uma lista de pessoas que praticaram o ato.

### Resposta de Sucesso

Será informado um resumo do selo salvo.

Código: 200

    {"mensagem":"Selo digital cadastrado com sucesso"}

### Resposta de Erro: 

Erro geral.

Código: 500

    {
        "erro":  "Ocorreu um erro no sistema"
    }
    
*OU*

Token inválido.

Código: 401

    {
        "erro":  "Token inválido"
    }

Erros de validação. Existem diferentes erros de validação.

Código: 400

    {
        "erro": "Erro de sintaxe. O campo \"xxxxx\" não foi encontrado."
    }

### Exemplo de Chamada

    POST /v2.0.1/selo/enviar HTTP/1.1
    Host: api.cartoriotocantins.com.br
    Content-Type: application/json
    Cache-Control: no-cache

    {
        "token": "abcdefghijklmnopqrstuvwxyz",
        "tipo": "AUTENTICACAO",
        "selo": "222222AAA000001-AAA",
        "atos": [{
            "codigo": 1004,
            "dados": [{
                "campo": "tipo_ato_cep",
                "valor": "2"
            },{
                "campo": "diferido",
                "valor": "false"
            },{
                "campo": "gratuito",
                "valor": "false"
            },{
                "campo": "livro",
                "valor": "2"
            }, {
                "campo": "folha",
                "valor": "65"
            }, {
                "campo": "observacao",
                "valor": "preenchi porque está obrigatório"
            }, {
                "campo": "valor_issqn",
                "valor": ""
            }, {
                "campo": "data_protocolo",
                "valor": "2017-02-06"
            }, {
                "campo": "data_ato",
                "valor": "2017-02-06"
            }, {
                "campo": "quantidade_pessoas",
                "valor": "1"
            }, {
                "campo": "detalhe",
                "valor": [{
                  "dados":[
                    {
                        "campo": "cpf",
                        "valor": "51715386388"
                    }, {
                        "campo": "qualidade",
                        "valor": "2"
                    }, {
                        "campo": "nome",
                        "valor": "Fulano da Silva"
                    }, {
                        "campo": "tipo_documento_1",
                        "valor": "CNPJ"
                    }, {
                        "campo": "numero_documento_1",
                        "valor": "854695"
                    }, {
                        "campo": "rg",
                        "valor": "563254"
                    }, {
                        "campo": "rg_orgao_emissor",
                        "valor": "SSP-TO"
                    }]
                }]
            }]
        }]
    }
