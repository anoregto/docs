# Solicitação de liberação do cartório

### Url

`/cartorio/liberar/{cns}`

### Método

`POST`

### Parametros de URL

`cns` - O cns do cartório.

### Parametros

* nome - O nome do contato técnico associado ao token.
* email - Um email para contato (deve ser informado um email válido).

    {
        "nome": "Fulano da Silva",
        "email": "fulado.silva@gmail.com"
    }

### Resposta de Sucesso

Código: 200

    {
      "mensagem": "Email com código de ativação enviado ao titular do cartório. O código tem validade de 24h."
    }

### Resposta de Erro: 

Erro geral.

Código: 500

    {
        "erro":  "Ocorreu um erro no sistema"
    }
    
*OU*

CNS incorreto.

Código: 404

    {
        "erro":  "Não foi possível encontrar o cartório."
    }

*OU*

Dados incorretos. Existem vários tipos de erros de dados inválidos.

Código: 400

    {
        "erro":  "Deve ser informado um CNPJ válido."
    }


### Exemplo de Chamada

    POST /v2.1.0/cartorio/liberar/222222 HTTP/1.1
    Host: api.cartoriotocantins.com.br
    Content-Type: application/json
    Cache-Control: no-cache

    {
        "nome": "Fulano da Silva",
        "email": "fulado.silva@gmail.com"
    }

### Observações

A liberação do cartório requer intervenção humana para verificação da autenticidade da solicitação.
Essa verificação é feita através de um email enviado ao email principal da serventia cadastrado no CNJ, assim o titular do cartório, que possuí acesso a esse email, deve repassar o token de ativação ao responsável pelo envio das informações.

Para efeito de testes, na versão SANDBOX o cartório CNS: 222222 pode ser usado, nesse caso o token de liberação da API será enviado diretamente ao email de contato informado.
