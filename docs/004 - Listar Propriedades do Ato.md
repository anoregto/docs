# Listar Propriedades do Ato

### Url

`/selo/campos/{codigo}`

### Método

`POST`

### Parametros de URL

`codigo` - O código do ato praticado segundo as especificações do GISE.

### Parametros

* token - O token de acesso do cartório;

    {
        "token": "coloque_o_token_aqui"
    }

### Resposta de Sucesso

Será retornado um objeto com os campos que devem ser fornecidos a Central.

Código: 200

    [
        {
            "nome": "diferido",
            "rotulo": "Diferido",
            "tipo": "BOLEANO",
            "valor_padrao": "false",
            "expressao_regular": ".*",
            "tamanho_maximo": 255,
            "tamanho_minimo": 0,
            "obrigatorio": "0",
            "opcoes": "",
            "subcampos": []
        },
        {
            "nome": "gratuito",
            "rotulo": "Gratuito",
            "tipo": "BOLEANO",
            "valor_padrao": "false",
            "expressao_regular": ".*",
            "tamanho_maximo": 255,
            "tamanho_minimo": 0,
            "obrigatorio": "0",
            "opcoes": "",
            "subcampos": []
        },
        {
            "nome": "observacao",
            "rotulo": "Observação",
            "tipo": "TEXTO_LONGO",
            "valor_padrao": "",
            "expressao_regular": ".*",
            "tamanho_maximo": 500,
            "tamanho_minimo": 0,
            "obrigatorio": "0",
            "opcoes": "",
            "subcampos": []
        },
        {
            "nome": "anexo",
            "rotulo": "Arquivo PDF/A do Ato",
            "tipo": "ANEXO",
            "valor_padrao": null,
            "expressao_regular": ".*",
            "tamanho_maximo": 0,
            "tamanho_minimo": 0,
            "obrigatorio": "0",
            "opcoes": "",
            "subcampos": []
        },
        {
            "nome": "conteudo_textual",
            "rotulo": "Conteúdo Textual do Ato",
            "tipo": "TEXTO_LONGO",
            "valor_padrao": null,
            "expressao_regular": ".*",
            "tamanho_maximo": 255,
            "tamanho_minimo": 0,
            "obrigatorio": "0",
            "opcoes": "",
            "subcampos": []
        },
        {
            "nome": "data_protocolo",
            "rotulo": "Data Protocolo",
            "tipo": "DATA",
            "valor_padrao": "now",
            "expressao_regular": ".*",
            "tamanho_maximo": 255,
            "tamanho_minimo": 0,
            "obrigatorio": "1",
            "opcoes": "",
            "subcampos": []
        },
        {
            "nome": "data_ato",
            "rotulo": "Data Ato",
            "tipo": "DATA",
            "valor_padrao": "now",
            "expressao_regular": ".*",
            "tamanho_maximo": 255,
            "tamanho_minimo": 0,
            "obrigatorio": "1",
            "opcoes": "",
            "subcampos": []
        },
        {
            "nome": "livro",
            "rotulo": "Livro",
            "tipo": "TEXTO_CURTO",
            "valor_padrao": "",
            "expressao_regular": ".*",
            "tamanho_maximo": 20,
            "tamanho_minimo": 0,
            "obrigatorio": "1",
            "opcoes": "",
            "subcampos": []
        },
        {
            "nome": "folha",
            "rotulo": "Folha",
            "tipo": "TEXTO_CURTO",
            "valor_padrao": "",
            "expressao_regular": ".*",
            "tamanho_maximo": 4,
            "tamanho_minimo": 0,
            "obrigatorio": "1",
            "opcoes": "",
            "subcampos": []
        },
        {
            "nome": "quantidade_pessoas",
            "rotulo": "",
            "tipo": "NUMERO_INTEIRO",
            "valor_padrao": "0",
            "expressao_regular": ".*",
            "tamanho_maximo": 255,
            "tamanho_minimo": 0,
            "obrigatorio": "0",
            "opcoes": "",
            "subcampos": []
        },
        {
            "nome": "detalhe",
            "rotulo": "Partes Envolvidas",
            "tipo": "ENVOLVIDO_CPF_CNPJ_TIPO_CONJUGUE",
            "valor_padrao": "",
            "expressao_regular": ".*",
            "tamanho_maximo": 255,
            "tamanho_minimo": 0,
            "obrigatorio": "0",
            "opcoes": "",
            "subcampos": [
                {
                    "nome": "cpf",
                    "rotulo": "CPF do Cônjuge",
                    "tipo": "CPF",
                    "valor_padrao": null,
                    "expressao_regular": ".*",
                    "tamanho_maximo": 255,
                    "tamanho_minimo": 0,
                    "obrigatorio": "1",
                    "opcoes": ""
                },
                {
                    "nome": "qualidade",
                    "rotulo": "Qualidade",
                    "tipo": "SELECT",
                    "valor_padrao": null,
                    "expressao_regular": ".*",
                    "tamanho_maximo": 255,
                    "tamanho_minimo": 0,
                    "obrigatorio": "0",
                    "opcoes": {
                        "1": "Outorgante",
                        "2": "Outorgado",
                        "3": "Representante",
                        "4": "Rogo",
                        "5": "Intimado",
                        "6": "Representado"
                    }
                },
                {
                    "nome": "nome",
                    "rotulo": "Nome",
                    "tipo": "TEXTO_CURTO",
                    "valor_padrao": null,
                    "expressao_regular": ".*",
                    "tamanho_maximo": 255,
                    "tamanho_minimo": 0,
                    "obrigatorio": "1",
                    "opcoes": ""
                },
                {
                    "nome": "tipo_documento_1",
                    "rotulo": "Tipo 1° Documento",
                    "tipo": "SELECT",
                    "valor_padrao": null,
                    "expressao_regular": ".*",
                    "tamanho_maximo": 4,
                    "tamanho_minimo": 0,
                    "obrigatorio": "1",
                    "opcoes": {
                        "CPF": "CPF",
                        "CNPJ": "CNPJ"
                    }
                },
                {
                    "nome": "numero_documento_1",
                    "rotulo": "Número 1° Documento",
                    "tipo": "TEXTO_CURTO",
                    "valor_padrao": null,
                    "expressao_regular": ".*",
                    "tamanho_maximo": 32,
                    "tamanho_minimo": 0,
                    "obrigatorio": "1",
                    "opcoes": ""
                },
                {
                    "nome": "rg",
                    "rotulo": "RG",
                    "tipo": "TEXTO_CURTO",
                    "valor_padrao": null,
                    "expressao_regular": ".*",
                    "tamanho_maximo": 32,
                    "tamanho_minimo": 0,
                    "obrigatorio": "0",
                    "opcoes": ""
                },
                {
                    "nome": "rg_orgao_emissor",
                    "rotulo": "Orgão Emissor",
                    "tipo": "TEXTO_CURTO",
                    "valor_padrao": null,
                    "expressao_regular": ".*",
                    "tamanho_maximo": 64,
                    "tamanho_minimo": 0,
                    "obrigatorio": "0",
                    "opcoes": ""
                }
            ]
        },
        {
            "nome": "tipo_ato_cep",
            "rotulo": "Tipo do Ato",
            "tipo": "SELECT",
            "valor_padrao": null,
            "expressao_regular": "[0-9]",
            "tamanho_maximo": 255,
            "tamanho_minimo": 0,
            "obrigatorio": "1",
            "opcoes": {
                "1": "Escritura",
                "2": "Procuração",
                "3": "Procuração para Fins Previdenciários",
                "5": "Renúncia de Procuração",
                "6": "Revogação de Procuração",
                "7": "Substabelecimento",
                "8": "Ata Notarial"
            },
            "subcampos": []
        },
        {
            "nome": "natureza_escritura",
            "rotulo": "Natureza Escritura",
            "tipo": "SELECT",
            "valor_padrao": null,
            "expressao_regular": "[0-9]+",
            "tamanho_maximo": 2,
            "tamanho_minimo": 0,
            "obrigatorio": "0",
            "opcoes": {
                "1": "ACORDO EXTRAJUDICIAL",
                "4": "ALIENAÇÃO FIDUCIÁRIA",
                "5": "CESSÃO",
                "6": "COMPRA E VENDA",
                "10": "CONFISSÃO DE DÍVIDA/DAÇÃO EM PAGAMENTO",
                "14": "DECLARAÇÃO",
                "15": "DECLARATÓRIA DE UNIÃO ESTÁVEL",
                "16": "DECLARATÓRIA DE UNIÃO ESTÁVEL HOMOAFETIVA",
                "17": "DESAPROPRIAÇÃO",
                "20": "DISSOLUÇÃO DE UNIÃO ESTÁVEL",
                "21": "DISTRATO",
                "22": "DOAÇÃO",
                "23": "EMANCIPAÇÃO",
                "24": "HIPOTECA",
                "25": "INCORPORAÇÃO",
                "26": "BEM DE FAMÍLIA",
                "28": "LOCAÇÃO",
                "30": "PACTO ANTENUPCIAL",
                "31": "PENHOR",
                "33": "PROMESSA DE CESSÃO DE DIREITOS AQUISITIVOS",
                "34": "QUITAÇÃO",
                "35": "RERRATIFICAÇÃO",
                "36": "RECONHECIMENTO DE PATERNIDADE",
                "38": "REGISTRO DE CHANCELA MECÂNICA",
                "39": "REMISSÃO DE FORO E LAUDÊMIOS",
                "43": "SEM VALOR DECLARADO",
                "45": "SERVIDÃO",
                "46": "USUFRUTO (reserva, instituição e renúncia)",
                "48": "CONDOMÍNIO",
                "49": "PARCELAMENTO",
                "50": "SOCIEDADE E FUNDAÇÕES",
                "51": "TRANSAÇÃO",
                "52": "DIREITO DE USO OU SUPERFICIE",
                "53": "DIVISÃO",
                "54": "FIANÇA",
                "55": "DIRETIVAS ANTECIPADAS DE VONTADE (testamento vital)",
                "56": "CONFERÊNCIA DE BENS",
                "57": "NOVAÇÃO",
                "58": "CRÉDITO COM GARANTIA",
                "59": "EMISSÃO DE CÉDULA",
                "60": "EMISSÃO DE DEBÊNTURES",
                "61": "REVOGAÇÃO",
                "62": "RENUNCIA DE DIREITOS HEREDITÁRIOS",
                "63": "COMODATO/MÚTUO",
                "70": "PRESTAÇÃO DE SERVIÇOS",
                "71": "ARRENDAMENTO MERCANTIL (LEASING)",
                "72": "CONCESSÃO DE DOMÍNIO",
                "74": "CONTRATO DE NAMORO",
                "75": "CONCILIAÇÃO",
                "76": "MEDIAÇÃO"
            },
            "subcampos": []
        },
        {
            "nome": "valor",
            "rotulo": "Valor",
            "tipo": "NUMERO_DECIMAL",
            "valor_padrao": "",
            "expressao_regular": ".*",
            "tamanho_maximo": 255,
            "tamanho_minimo": 0,
            "obrigatorio": "0",
            "opcoes": "",
            "subcampos": []
        },
        {
            "nome": "natureza_litigio",
            "rotulo": "Natureza Litígio",
            "tipo": "SELECT",
            "valor_padrao": null,
            "expressao_regular": "[0-9]",
            "tamanho_maximo": 255,
            "tamanho_minimo": 0,
            "obrigatorio": "0",
            "opcoes": {
                "1": "Bancário",
                "2": "Concessionária de Água",
                "3": "Concessionária de Gás",
                "4": "Concessionária de Luz",
                "5": "Consumidor",
                "6": "Contrato",
                "7": "Empresarial",
                "8": "Família",
                "9": "Locação",
                "10": "Mobiliário",
                "11": "Previdência",
                "12": "Saúde",
                "13": "Seguro",
                "14": "Serviço Público",
                "15": "Sucessões",
                "16": "Telefonia",
                "17": "Transporte",
                "18": "Transporte – Avião",
                "19": "Transporte – Barco",
                "20": "Transporte – Metrô",
                "21": "Transporte - Ônibus"
            },
            "subcampos": []
        },
        {
            "nome": "acordo",
            "rotulo": "Acordo",
            "tipo": "SELECT",
            "valor_padrao": null,
            "expressao_regular": "(SIM|NÃO)",
            "tamanho_maximo": 255,
            "tamanho_minimo": 0,
            "obrigatorio": "0",
            "opcoes": {
                "SIM": "Sim",
                "NÃO": "Não"
            },
            "subcampos": []
        },
        {
            "nome": "desconhecido",
            "rotulo": "Desconhecido",
            "tipo": "TEXTO_LONGO",
            "valor_padrao": null,
            "expressao_regular": ".*",
            "tamanho_maximo": 255,
            "tamanho_minimo": 0,
            "obrigatorio": "0",
            "opcoes": "",
            "subcampos": []
        },
        {
            "nome": "referente_livro",
            "rotulo": "Referente Livro",
            "tipo": "TEXTO_CURTO",
            "valor_padrao": null,
            "expressao_regular": "^([0-9]{0,3})[A-Za-z]?$",
            "tamanho_maximo": 255,
            "tamanho_minimo": 0,
            "obrigatorio": "0",
            "opcoes": "",
            "subcampos": []
        },
        {
            "nome": "referente_folha",
            "rotulo": "Referente Folha",
            "tipo": "TEXTO_CURTO",
            "valor_padrao": null,
            "expressao_regular": "^([0-9]{0,3})[A-Za-z]?$",
            "tamanho_maximo": 255,
            "tamanho_minimo": 0,
            "obrigatorio": "0",
            "opcoes": "",
            "subcampos": []
        },
        {
            "nome": "referente_cns",
            "rotulo": "Referente Cns",
            "tipo": "TEXTO_CURTO",
            "valor_padrao": null,
            "expressao_regular": "^([0-9]{6})?$",
            "tamanho_maximo": 255,
            "tamanho_minimo": 0,
            "obrigatorio": "0",
            "opcoes": "",
            "subcampos": []
        }
    ]

### Resposta de Erro: 

Erro geral.

Código: 500

    {
        "erro":  "Ocorreu um erro no sistema"
    }
    
*OU*

Token inválido.

Código: 401

    {
        "erro":  "Token inválido"
    }

### Exemplo de Chamada

    POST /v2.1.0/selo/campos/1004 HTTP/1.1
    Host: api.cartoriotocantins.com.br
    Content-Type: application/json
    Cache-Control: no-cache

    {"token":"coloque_o_token_aqui"}
