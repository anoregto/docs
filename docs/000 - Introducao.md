Central de Informações dos Cartórios
================================================================

Especificações da API de acordo com o formato exposto em [REST API Documentation Best Practices](https://bocoup.com/weblog/documenting-your-api), também pode ser visto no [repositório da documentação](https://bitbucket.org/cartoriovirtual/docs/).

Devido as grandes dificuldades encontradas para consumo da API ela foi simplificada.
