# Solicitação de liberação do cartório

### Url

`/cartorio/ativar`

### Método

`POST`

### Parametros

* token - O token a ser ativado.

    {
        "token": "coloque_o_token_aqui"
    }

### Resposta de Sucesso

Código: 200

    {
      "valor": "coloque_o_token_aqui",
      "validade": "2018-02-13 21:11:59",
      "situacao": "ATIVO"
    }

### Resposta de Erro: 

Erro geral.

Código: 500

    {
        "erro":  "Ocorreu um erro no sistema"
    }
    
*OU*

Token inválido.

Código: 401

    {
        "erro":  "Token inválido"
    }

*OU*

Token já ativado

Código: 400

    {
        "erro":  "Token já ativado"
    }


### Exemplo de Chamada

    POST /v2.1.0/cartorio/ativar HTTP/1.1
    Host: api.cartoriotocantins.com.br
    Content-Type: application/json
    Cache-Control: no-cache

    {"token": "sl9PXtQx0CTMixVlc0DgsAkENocMsfHyrJNHOwe9oseL8Uv7XPiZcmWlaIkJQ0N6sTcpG4ajltjU1SdaB3s"}

### Observações

A liberação do cartório requer intervenção humana para verificação da autenticidade da solicitação.
Essa verificação é feita através de um email enviado ao email principal da serventia cadastrado no CNJ, assim o titular do cartório, que possuí acesso a esse email, deve repassar o token de ativação ao responsável pelo envio das informações.
